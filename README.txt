This is a command line program. The syntax for the arguments is:
"srget -o <output file> [-c [<numConn>]] http://someurl.domain[:port]/path/to/file"
The output file will be written to the directory where the .py file is located.

[REQUIRE] Python 2.7, use other Python version AT YOUR OWN RISK.

The program will first check for any redirects, by sending a HEAD request.
It will also check if range is supported and whether it is chunked transfer encoding.

If resume is supported, a ".inprogress" file will be created.
The header will be written to this file.
Additionally, ".info" and ".store" files will be created.
The amount of data downloaded will be store there as data is written to file. 

The program will then proceed to download the file.

If a existing download is detected and with the user's permission, the program will attempt to continue the download.
The program will check the local header with the one from server, resuming only if the last-modified header match.
If not, the program will redownload the file from the beginning.

If user specify more than one connection, and the server support it, work will be calculated.
Each thread will get equal amount of work and number of threads will be equal to number of connections.
In certain cases, number of threads will be less than number of connections as remaining work is too low.

Once downloading is completed, program will terminate.
If the transfer-encoding is chunked, it will be decoded before terminating.