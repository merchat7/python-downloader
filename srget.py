from urlparse import urlparse
from httplib import HTTPResponse
from StringIO import StringIO
import time, pickle, sys, os, glob, getopt, threading, socket as s

#for parsing the header, code from http://stackoverflow.com/questions/24728088/python-parse-http-response-string
class FakeSocket():
    def __init__(self, response_str):
        self._file = StringIO(response_str)
    def makefile(self, *args, **kwargs):
        return self._file

class robustDownload(threading.Thread):
    def __init__(self, server, object , port, outFile, rangeBool, info):
        threading.Thread.__init__(self)
        self.server = server
        self.object = object
        self.port = port
        self.outFile = outFile
        self.rangeBool = rangeBool
        self.info = info

    def run(self):
        timeoutCount = 0
        global terminate
        while self.info:
            info = self.info.pop()
            resumePoint = info[0]
            bitLength = info[1]
            number = info[2]
            progress = 0
            while True :
                if progress == bitLength:
                    break
                if timeoutCount >= 5:
                    terminate = True
                    sys.exit(2) #kill thread
                try:
                    downloaded = getFile(self.server,self.object,self.port,self.outFile,self.rangeBool,resumePoint,number)
                    if downloaded != 0:
                        timeoutCount = 0 #we manage to download something so reset timeout
                        progress += downloaded

                    if progress < bitLength:
                        start,end = resumePoint.split("-",1)
                        start = int(start) + downloaded
                        resumePoint = "{i}-{o}".format(i=start,o=end)
                        raise Exception #for timeout Count
                    elif progress > bitLength:
                        print "MASSIVE ERROR ALERT"
                        terminate = True
                        sys.exit(2)
                except:
                    timeoutCount += 1
                    time.sleep(3) #try again after 3 seconds
        threadDone.append(1)

def main(arguments):
    numConn = 1
    try:
        commands,website = getopt.getopt(arguments,"o:c:")
    except getopt.GetoptError:
        print "ERROR: Invalid syntax\nUsage: srget -o <output file> [-c [<numConn>]] http://someurl.domain[:port]/path/to/file"
        sys.exit(2)

    if len(arguments) == 0 or len(website) > 1: #no argument or too many arguments
        print "ERROR: Invalid syntax\nUsage: srget -o <output file> [-c [<numConn>]] http://someurl.domain[:port]/path/to/file"
        sys.exit(2)

    if len(website) == 0:
        print "ERROR: No URL specified"
        sys.exit(2)

    if "-o" not in arguments:
        print "ERROR: No output file specified"
        sys.exit(2)

    for opt,arg in commands:
        if opt == "-o":
            outputFile = arg
        elif opt == "-c":
            if not arg.isdigit():
                print "ERROR: numConn isn't a number"
                sys.exit(2)
            numConn = int(arg)
    return outputFile, numConn, website[0]

def decodeChunked(data,outputFile):
    actualData = ""
    offset = 0
    while offset < len(data):
        index = data[offset:].index("\r\n") + offset
        bitLength = int(data[offset:index],16) + len(data[offset:index]) + offset + 2
        actualData += data[index+2:bitLength]
        offset = len(data[:bitLength + 2])
    try:
        file = open(outputFile,"wb")
        file.write(actualData)
        file.close()
    except:
        print "\nError opening file, exiting..."
        sys.exit(2)

def makeDownloadRequest(servName, objName, rangeByte):
    return ("GET {o} HTTP/1.1\r\n" + "Host:{s}\r\n" + "Connection: close\r\n" + "Accept-Encoding:\r\n" + rangeByte + "\r\n\r\n").format(o=objName, s=servName)

def makeHeaderRequest(servName, objName):
    return ("HEAD {o} HTTP/1.1\r\n" + "Host:{s}\r\n" + "Connection: close\r\n" + "Range: bytes=0-" + "\r\n\r\n").format(o=objName, s=servName)

def isChunkedChecker(servName, objName):
    sock = s.socket(s.AF_INET, s.SOCK_STREAM)
    try:
        sock.connect((servName, 80))
    except:
        print "\nError connecting to server\nNo response from server or invalid address"
        sys.exit(2)
    request = makeDownloadRequest(servName, objName, "")
    sock.settimeout(5)
    sock.send(request)
    sock.settimeout(None)
    header = ""
    isChunked = False
    while True:
        try:
            sock.settimeout(5)
            data = sock.recv(8196)
        except:
            print "Connection timed out, exiting..."
            sys.exit(2)
        if data:
            sock.settimeout(None)
            header += data
            if "\r\n\r\n" in header:
                head,content = header.split("\r\n\r\n",1)
                response5 = HTTPResponse(FakeSocket(head))
                response5.begin()
                if response5.getheader("Transfer-Encoding") == "chunked":
                    isChunked = True
                sock.close()
                break
    return isChunked

def getHeader(servName, objName):
    sock = s.socket(s.AF_INET, s.SOCK_STREAM)
    try:
        sock.connect((servName, 80))
    except:
        print "\nError connecting to server\nNo response from server or invalid address"
        sys.exit(2)
    request = makeHeaderRequest(servName, objName)
    sock.settimeout(5)
    sock.send(request)
    sock.settimeout(None)
    header = ""
    while True:
        try:
            sock.settimeout(5)
            data = sock.recv(8196)
        except:
            print "Connection timed out, exiting..."
            sys.exit(2)
        if data:
            sock.settimeout(None)
            header += data
        else:
            sock.close()
            break
    return header

def getFile(servName, objName, portNumber,outputFile,rangeBoolean,resumePoint,numInfo):
    downloadSoFar = 0
    global terminate
    if not resumePoint: #just for printing
        resumePoint = "0-"
    if rangeBoolean:
        startPoint = int(resumePoint.split("-",1)[0])
        try:
            infoFile = open(outputFile + "." + str(numInfo) + ".info","wb")
            infoFile.write("{a},{b}".format(a=startPoint,b=startPoint+downloadSoFar-1))
            infoFile.close()
        except:
            print "[Range: "+resumePoint+"]\n ","Failed to create necessary file, exiting..."
            terminate = True
            sys.exit(2)
    sock = s.socket(s.AF_INET, s.SOCK_STREAM)
    try:
        sock.connect((servName, portNumber))
    except:
        print "[Range: "+resumePoint+"]\n ","Error connecting to server, exiting..."
        if rangeBoolean:
            return downloadSoFar
        sys.exit(2)
    if rangeBoolean:
        request = makeDownloadRequest(servName, objName, "Range: bytes={b}".format(b=resumePoint))
        try:
            file = open(outputFile, "r+b")
        except:
            print "[Range: "+resumePoint+"]\n ","Error opening file, exiting..."
            terminate = True
            sys.exit(2)
    else:
        request = makeDownloadRequest(servName, objName, "")
        try:
            file = open(outputFile,"wb") #not resuming, since range not supported, so wb is fine
        except:
            print "[Range: "+resumePoint+"]\n ","Error creating file, exiting..."
            sys.exit(2)
    sock.settimeout(5)
    sock.send(request)
    sock.settimeout(None)
    header = ""
    writeToDisk = False
    while True:
        try:
            sock.settimeout(10)
            data = sock.recv(8196)
        except:
            print "[Range: "+resumePoint+"]\n ", "Connection timed out, exiting..."
            if rangeBoolean:
                return downloadSoFar
            sys.exit(2)
        if data:
            sock.settimeout(None)
            if writeToDisk:
                file.write(data)
                if rangeBoolean:
                    try:
                        infoFile = open(outputFile + "." + str(numInfo) + ".info","r+b")
                        offset = infoFile.read().find(",")+1 #+1 the index of where we found ","
                        infoFile.seek(offset)
                        downloadSoFar += len(data)
                        infoFile.write(str(startPoint+downloadSoFar-1)) #e.g. downloadSoFar = 100, => 0 - 99
                        infoFile.close()
                    except:
                        print "[Range: "+resumePoint+"]\n ","Failed to update necessary file, exiting..."
                        terminate = True
                        sys.exit(2)
            else:
                header += data
                if "\r\n\r\n" in header:
                    print "[Range: "+resumePoint+"]\n ","Header retrieved, isolating...",
                    head,content = header.split("\r\n\r\n",1)
                    response2 = HTTPResponse(FakeSocket(head))
                    response2.begin()
                    if response2.status == 416:
                        print "[Range: "+resumePoint+"]\n ","ERROR: Server send requested range not satisfiable"
                        terminate = True
                        sys.exit(2)
                    response2.close()
                    if rangeBoolean:
                        file.seek(startPoint)
                    else:
                        file.seek(0,2) #seek to end of file
                    file.write(content)
                    if rangeBoolean:
                        try:
                            infoFile = open(outputFile + "." + str(numInfo) + ".info","r+b")
                            offset = infoFile.read().find(",")+1
                            infoFile.seek(offset)
                            downloadSoFar += len(content)
                            infoFile.write(str(startPoint+downloadSoFar-1))
                            infoFile.close()
                        except:
                            print "[Range: "+resumePoint+"]\n ","Failed to update necessary file, exiting..."
                            terminate = True
                            sys.exit(2)
                    header = ""
                    writeToDisk = True
                    print "Done\n{f}  Continuing with download...".format(f="[Range: "+resumePoint+"]\n")
        else:
            sock.close()
            break
    file.close()
    print "[Range: "+resumePoint+"]\n  Completed"
    if rangeBoolean:
        return downloadSoFar

def calculateWork(outputFile):
    global numConn
    filesInfo = []
    startLst = []
    endLst = []
    alreadyDownload = 0
    if glob.glob("*.info") or os.path.isfile(outputFile + ".store"):
        if glob.glob("*.info"):
            for infoFileName in glob.glob("*.info"):
                try:
                    infoFile = open(infoFileName,"rb")
                    start,end = infoFile.read().split(",",1)
                    infoFile.close()
                except:
                    print "Failed to open necessary file, exiting..."
                    sys.exit(2)
                if int(start) == int(end)+1: #means we haven't download anything
                    continue
                progress = int(end)-int(start)
                alreadyDownload += progress + 1 #eg 0 - 99 = 100 already download, so +1
                startLst.append(int(start))
                endLst.append(int(end))
        if os.path.isfile(outputFile + ".store"):
            try:
                oldStart,oldEnd = pickle.load(open(outputFile + ".store","rb"))
            except:
                print "Failed to open necessary file, exiting..."
                sys.exit(2)
            for n in range(len(oldStart)):
                progress = int(oldEnd[n]) - int(oldStart[n])
                alreadyDownload += progress + 1
                startLst.append(oldStart[n])
                endLst.append(oldEnd[n])
        startLst.sort()
        endLst.sort()
        store = [startLst,endLst]
        if startLst:
            try:
                pickle.dump(store, open(outputFile + ".store","wb"))
            except:
                print "Failed to store necessary file, exiting..."
                sys.exit(2)
        #clean-up what we've dumped
        for infoFileName in glob.glob("*.info"):
            try:
                os.remove(infoFileName)
            except:
                print "Failed to remove necessary file, exiting..."
                sys.exit(2)
        #determine where to download
        for i in range(len(endLst)):
            start = int(endLst[i]) + 1 #e.g. 0-99 already downloaded, so must start from 100
            if i+1 != len(startLst):
                end = int(startLst[i+1]) - 1
                size = end - start + 1
                if not start > end:
                    filesInfo.append(["{i}-{o}".format(i=start,o=end),size])
            else:
                if int(start) != fileSize:
                    size = int(fileSize) - start
                    filesInfo.append(["{i}-".format(i=start),size])
        remainingWork = fileSize - alreadyDownload +1
        if numConn > remainingWork:
            numConn = int(remainingWork)
        partLength = round((remainingWork)/float(numConn))
        done = []
        #ensure each thread get equal work
        while filesInfo:
            info = filesInfo.pop()
            work = []
            if info[1] == partLength: #base case
                work.append(info)
                done.append(work)
            elif info[1] > partLength:
                difference = info[1] - int(partLength)
                start,end = info[0].split("-",1)
                if end:
                    work.append(["{i}-{o}".format(i=start,o=int(end)-difference),partLength])
                    filesInfo.append(["{i}-{o}".format(i=int(end) - difference+1,o=end),difference])
                else:
                    end = int(fileSize) - 1
                    work.append(["{i}-{o}".format(i=start,o=end-difference),partLength])
                    filesInfo.append(["{i}-".format(i=end - difference+1),difference])
                done.append(work)
            else: #if smaller, need to add more work
                size = info[1] #add to this till size == partLength
                updatedLength = partLength - size #the remaining work to make size == partLength
                work.append(info)
                if not filesInfo:
                    done.append(work)
                while filesInfo:
                    info = filesInfo.pop()
                    size += info[1]
                    if info[1] == updatedLength: #nothing left, give all remaining work to last thread
                        work.append(info)
                        done.append(work)
                        break
                    elif info[1] < updatedLength: #still need more work
                        updatedLength -= info[1] #how much more work we still need
                        work.append(info)
                    else:
                        difference2 = info[1] - int(updatedLength)
                        start,end = info[0].split("-",1)
                        if end:
                            work.append(["{i}-{o}".format(i=start,o=int(end)-difference2),updatedLength])
                            filesInfo.append(["{i}-{o}".format(i=int(end) - difference2+1,o=end),difference2])
                        else:
                            end = int(fileSize)  - 1
                            work.append(["{i}-{o}".format(i=start,o=end-difference2),updatedLength])
                            filesInfo.append(["{i}-".format(i=end - difference2+1),difference2])
                        done.append(work)
                        break
        filesInfo = done
    else:
        if numConn > fileSize:
            numConn = int(fileSize)
        partLength = round(float(fileSize)/numConn)
        lastLength = fileSize - partLength * (numConn-1)
        index = 0
        for n in xrange(numConn):
            initialLength = partLength * index
            if index != numConn-1:
                filesInfo.append([["{i}-{o}".format(i=int(initialLength),o=int(initialLength+partLength-1)),int(partLength)]])
            else:
                filesInfo.append([["{i}-".format(i=int(initialLength)),int(lastLength)]])
            index += 1
    return filesInfo

def removeFiles():
    if os.path.isfile(outputFile + ".inprogress"):
        try:
            os.remove(outputFile + ".inprogress")
        except:
            print "Failed to remove necessary file, exiting..." #incase file lead to some glitchy behavior
            sys.exit(2)
    if os.path.isfile(outputFile + ".store"):
        try:
            os.remove(outputFile + ".store")
        except:
            print "Failed to remove necessary file, exiting..." #incase file lead to some glitchy behavior
            sys.exit(2)
    for infoFileName in glob.glob("*.info"):
        try:
            os.remove(infoFileName)
        except:
            print "Failed to remove necessary file, exiting..." #incase file lead to some glitchy behavior
            sys.exit(2)

if __name__ == "__main__":
    outputFile, numConn, siteName = main(sys.argv[1:])

if "https" in siteName:
    print "ERROR: https protocol not supported, exiting..."
    sys.exit(2)
elif "http://" not in siteName:
    print "Invalid URL, ensure your URL is in the format \"http://...\""
    sys.exit(2)

threadDone = []
port = None
resumeOK = False
terminate = None

if os.path.isfile(outputFile):
    while True:
        print "File already exist, are you sure you want to continue? (y/n):",
        user_input = raw_input()
        if user_input in ["y","Y","yes","YES","Yes"]:
            break
        elif user_input in ["n","N", "no","NO","No"]:
            print "Exiting..."
            sys.exit(2)
        else:
            print "Please type either y or n"

if os.path.isfile(outputFile + ".inprogress") or os.path.isfile(outputFile + ".store"):
    while True:
        if not os.path.isfile(outputFile): #prevent case where original file is missing = cannot resume
            removeFiles()
            try:
                blank = open(outputFile,"wb")
                blank.close()
                break
            except:
                print "Failed to create necessary file, exiting..."
                sys.exit(2)
        print "Partial download detected, attempt to resume? (y/n):",
        user_input = raw_input()
        if user_input in ["y","Y","yes","YES","Yes"]:
            resumeOK = True
            break
        elif user_input in ["n","N", "no","NO","No"]:
            try:
                blank = open(outputFile,"wb")
                blank.close()
            except:
                print "Failed to create necessary file, exiting..."
                sys.exit(2)
            removeFiles()
            break
        else:
            print "Please type either y or n"
else:
    try:
        #no resumeable download detected, create a new blank file to work with
        blank = open(outputFile,"wb")
        blank.close()
    except:
        print "Failed to create necessary file, exiting..."
        sys.exit(2)

print "Checking for redirects..."
while True:
    url = urlparse(siteName)
    if url.scheme == "https":
        print "ERROR: https protocol not supported, exiting..."
        sys.exit(2)
    server = url.hostname
    object = url.path or "/" #no path means download the page
    if url.query:
        object += "?" + url.query
    if not port: #ensure upon redirect, user-defined port is saved
        port = url.port or 80
    firstHead = getHeader(server,object)
    response = HTTPResponse(FakeSocket(firstHead))
    response.begin()
    if response.getheader("Content-Length"):
        fileSize = long(response.getheader("Content-Length")) #long because os.path.getsize return long
    else:
        fileSize = None
    status = response.status
    newAddress = response.getheader("Location")
    response.close()
    if status in [301,302,303,307]:
        if siteName == newAddress:
            print "ERROR: Redirecting to same location, exiting..."
            sys.exit(2)
        siteName = newAddress
        print "Redirecting...",
    elif status == 206:
        rangeSupport = True
        print "[Info] Resume supported"
        if not os.path.isfile(outputFile + ".inprogress"): #meaning we aren't trying to continue a download
            try:
                headFile = open(outputFile + ".inprogress","wb")
                headFile.write(firstHead)
                headFile.close()
            except:
                print "Error writing header to disk\nResume will not be supported"
                rangeSupport = False
                #Do nothing as if you cannot open the file, you cannot remove the file either
        break
    elif status in [200,416]: #e.g. Content-Range: */0, but file is downloadable without sending range
        rangeSupport = False
        print "[Info] Resume not supported"
        break
    else:
        print "ERROR: Unsupported HTTP status, exiting..."
        sys.exit(2)
print "Checking Done"

isChunked = isChunkedChecker(server, object)

if rangeSupport:
    if resumeOK:
        print "Attempting to resume"
        #check if local header matched with server
        header = firstHead
        response3 = HTTPResponse(FakeSocket(header))
        response3.begin()
        lastModifiedNew = response3.getheader("Last-Modified")
        response3.close()
        #we know request is valid from earlier error handling, so no error handling here
        try:
            headFile = open(outputFile + ".inprogress","rb")
            header2 = headFile.read()
            headFile.close()
        except:
            header2 = "" #incase opening .inprogress file fail
            print "Error retrieving header from local file\nResume will not be supported"
        try:
            response4 = HTTPResponse(FakeSocket(header2))
            response4.begin()
            lastModifiedOld = response4.getheader("Last-Modified")
            response4.close()
        except:
            lastModifiedOld = "doesn't exist"
        if lastModifiedOld == lastModifiedNew:
            print "Last modified date matched with server\nResuming download..."
        else:
            print "Local header didn't matched with server or was corrupted\nRestarting download..."
            removeFiles()
    else:
        print "Starting download..."
    filesInfo = calculateWork(outputFile)
    if not filesInfo: #no more to download
        if os.path.getsize(outputFile) != fileSize:
            print "Something went wrong, falling to fail-safe..."
            getFile(server,object,port,outputFile,False,None,None)
            print "Success"

    else:
        threadLst = []
        n = 1
        for info in filesInfo:
            for data in info:
                data.append(n)
                n += 1
        for info in filesInfo:
            threadLst.append(robustDownload(server,object,port,outputFile,True,info))
        for thread in threadLst:
            thread.daemon = True
            thread.start()
        while len(threadDone) != numConn:
            if terminate:
                print "Connection timed out, exiting..."
                sys.exit(2)
            time.sleep(1)
    removeFiles()
else:
    if resumeOK:
        print "Attempted to resume, but resume is not supported\nRestarting downloading..."
    elif numConn > 1:
        print "Server doesn't support multiple connections, downloading with 1 connection..."
    else:
        print "Starting download..."
    getFile(server,object,port,outputFile,False,None,None)
    removeFiles() #clean-up just incase

if isChunked:
    print "Decoding chunks...",
    fileSize = None #to ignore next check
    try:
        file = open(outputFile,"rb")
        data = file.read()
        file.close()
    except:
        print "\nError opening file, exiting..."
        sys.exit(2)
    decodeChunked(data,outputFile)
    print "Done"

errorSizeMessage = ""

if fileSize:
    if fileSize != os.path.getsize(outputFile):
        errorSizeMessage = ", but file size doesn't match content-length\n[Warning] Something may have went wrong"
else:
    #we cannot check file integrity so we ask user to do so (for e.g. chunked transfer encoding
    errorSizeMessage = ", but unable to verify integrity of the file\n[Info] Please manually check the file to ensure file integrity"
print "Download completed" + errorSizeMessage + "\nExiting..."
